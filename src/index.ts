function removeAllBefore(values: number[], b: number): number[] {
  console.log(values.indexOf(b) !== -1);
  return values.indexOf(b) !== -1 ? values.slice(values.indexOf(b)) : [];
}

console.log(removeAllBefore([1, 2, 3, 4, 5], 3));
console.log(removeAllBefore([], 0), []);

console.log('test a');

export default removeAllBefore;
